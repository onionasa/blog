from django.conf import settings
from django.contrib.auth.decorators import login_required
from rest_framework.routers import SimpleRouter
from blogsite.views import *

__author__ = 'AsA'

from django.conf.urls import include, url, patterns
from . import views
router = SimpleRouter()
router.register(r'category', CategoryApi,base_name="category_api")
router.register(r'catmanager',CategoryManagerApi,base_name="cat_manager_api")
router.register(r'post', PostApi,base_name="post_api")
router.register(r'postmanager',PostManagerApi,base_name="post_manager_api")
router.register(r'login', LoginApi,base_name="login_api")
router.register(r'logout', LogoutApi,base_name="logout_api")
router.register(r'user_register',RegisterApi,base_name="register_api")
router.register(r'catpost/(?P<ck>[0-9]+)', CategoryPostApi,base_name="catpost_api")
router.register(r'postdetail/(?P<ck>[0-9]+)', PostDetailApi,base_name="post_detail")
router.register(r'comment/(?P<ck>[0-9]+)', CommentApi,base_name="comment_api")
router.register(r'comment_verify/(?P<ck>[0-9]+)/(?P<pk>[0-9]+)', CommentVerifyApi,base_name="comment_verify_api")
router.register(r'comment_delete/(?P<ck>[0-9]+)/(?P<pk>[0-9]+)', CommentDeleteApi,base_name="comment_delete_api")
urlpatterns = [

    url(r'^api/',include(router.get_urls())),
########AUTH OPERATIONS#####################################
    url(r'^api/$',views.ApiView.as_view(),name="api_view"),
    url(r'^apidoc/$',views.ApiDocView.as_view(),name="apidoc_view"),
    url(r'^register/$', views.RegisterView.as_view(), name="register"),
    url(r'^settings/$', login_required(views.Settings.as_view()), name="settings"),
    url(r'^login/$', views.UserLogin.as_view(), name='login'),
    url(r'^logout/$', views.user_logout, name='logout'),
########CATEGORY OPERATIONS#####################################
    url(r'^cat_manager/$',login_required(views.ShowCatEditor.as_view()),name='cat_manager'),
    url(r'^cat_add_edit/$',login_required(views.AddNewCat.as_view()),name='cat_add'),
    url(r'^cat_add_edit/(?P<cat_slug>[\w\-]+)/$',login_required(views.EditCat.as_view()),name='cat_edit'),
    url(r'^cat_delete/(?P<cat_slug>[\w\-]+)/$',login_required(views.DeleteCat.as_view()),name='cat_delete'),
########POST OPERATIONS#####################################
    url(r'^post_manager/$',login_required(views.ShowPostEditor.as_view()),name='post_manager'),
    url(r'^post_add/$',login_required(views.AddNewPost.as_view()),name='post_add'),
    url(r'^post_edit/(?P<pk>[0-9]+)/$',login_required(views.EditPost.as_view()),name='post_edit'),
    url(r'^post_delete/(?P<pk>[0-9]+)/$',login_required(views.DeletePost.as_view()),name='post_delete'),
    url(r'^comment_verify/(?P<cat_slug>[\w\-]+)/(?P<pka>[0-9]+)/(?P<pkb>[0-9]+)/$',login_required(views.CommentVerify.as_view()),name='comment_verify'),
    url(r'^comment_delete/(?P<cat_slug>[\w\-]+)/(?P<pka>[0-9]+)/(?P<pkb>[0-9]+)/$',login_required(views.CommentDelete.as_view()),name='comment_delete'),
###############PUBLIC URLS###################################
    url(r'^category/(?P<cat_slug>[\w\-]+)/$',views.ShowPostCatList.as_view(),name='post_cat_list'),
    url(r'^category/(?P<cat_slug>[\w\-]+)/(?P<pk>[0-9]+)$',views.ShowPostDetail.as_view(),name='post_detail'),
    url(r'^$',views.ShowPostList.as_view(),name='post_list'),
]

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
                            (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
                                'document_root': settings.MEDIA_ROOT}))
