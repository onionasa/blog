from datetime import datetime
from django.contrib import messages
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from django.db import IntegrityError
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.template.defaultfilters import slugify

from django.views.generic import View, CreateView
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ParseError
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from rest_framework.viewsets import ViewSet
from blogsite.models import Category, Posts, Comments, UserData
from blogsite.serializers import *

from .forms import UserForm, UserProfileForm, CategoryAddEditForm, PostAddEditForm, CommentForm, SettingsForm

#################USER AUTH PROCESSES###################

class IndexView(View):
    def dispatch(self, request, *args, **kwargs):
        return render(request,"index.html")


class RegisterView(View):

    def post(self,request,*args,**kwargs):

        registered = False
        instance=None
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            profile = profile_form.save(commit=False)
            profile.user = user
            if 'profile_image' in request.FILES:
                profile.profile_image = request.FILES['profile_image']
                profile.save()
                instance=profile.profile_image.url
            else:
                instance="/"
            profile.save()
            registered = True
        else:
            print user_form.errors, profile_form.errors
            return render(request,
            'register.html',
            {'user_form': user_form, 'profile_form': profile_form, 'registered': registered,"errora":user_form.errors,"errorb":profile_form.errors}
            )
        return HttpResponseRedirect('/')

    def get(self,request,*args,**kwargs):
        registered = False
        instance=None
        user_form = UserForm()
        profile_form = UserProfileForm()
        return render(request,
            'register.html',
            {'user_form': user_form, 'profile_form': profile_form, 'registered': registered,"kel":instance}
            )


@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/')



class UserLogin(View):
    def post(self, request, *args, **kwargs):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                return HttpResponse("account is disabled.")
        else:
            messages.add_message(request, messages.INFO, 'Invalid user information')
            return HttpResponseRedirect('/login/')
    def get(self, request, *args, **kwargs):
        return render(request,'login.html', {})




#Views User Settings
class Settings(View):

    def post(self,request,*args,**kwargs):

        registered = False
        instance=None
        user_form = UserForm(data=request.POST,instance=request.user)
        profile_form = UserProfileForm(data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            usr=request.user
            usr.username=request.user.username
            usr.set_password(request.POST.get('password'))
            usr.first_name=request.POST.get('first_name')
            usr.last_name=request.POST.get('last_name')
            usr.email=request.POST.get('email')
            usr.save()
            profile = UserData.objects.filter(user=usr)[0]
            if 'profile_image' in request.FILES:
                profile.profile_image = request.FILES['profile_image']
                profile.save()
                instance=profile.profile_image.url
            else:
                instance="/"
            profile.web=request.POST.get('web')
            profile.save()
            registered = True
        else:
            print user_form.errors, profile_form.errors
            messages.add_message(request, messages.WARNING, user_form.errors)
            messages.add_message(request, messages.INFO, profile_form.errors)
            return HttpResponseRedirect('/settings/')
        return HttpResponseRedirect('/')

    def get(self,request,*args,**kwargs):
        registered = False
        instance=None

        user_form = SettingsForm(initial={'username':request.user.username,
                                          'password':request.user.password,
                                          'first_name':request.user.first_name,
                                          'last_name':request.user.last_name,
                                          'email':request.user.email
                                          })
        prf= UserData.objects.get(user=request.user)
        profile_form = UserProfileForm(initial={'web':prf.web,'profile_image':prf.profile_image})
        return render(request,
            'settings.html',
            {'user_form': user_form, 'profile_form': profile_form, 'registered': registered,"kel":instance}
            )


############################CATEGORY PROCESSES#########################################################

#Views Category List vie Table
class ShowCatEditor(View):
    def dispatch(self, request, *args, **kwargs):
        cat=Category.objects.filter(cat_user=request.user.id)
        return render(request,"cat_manager.html",{"categories":cat})
#Views add category form
class AddNewCat(CreateView):
    template_name="cat_add_edit.html"
    form_class=CategoryAddEditForm
    def form_valid(self, form):
        try:
            a = form.save(commit=False)
            a.cat_user = self.request.user
            a.cat_slug = slugify(unicode(a.cat_name))
            a.save()
            return HttpResponseRedirect('/cat_manager/')
        except IntegrityError as e:
            return HttpResponse("Category already exist")

#Views category edit form, template same as Add only values init
class EditCat(View):
    def post(self,request,*args,**kwargs):
        csform = CategoryAddEditForm(data=request.POST)
        if csform.is_valid():
            t=Category.objects.filter(cat_slug=kwargs['cat_slug'])[0]
            t.cat_user=request.user
            t.cat_name=request.POST.get('cat_name')
            t.cat_slug=slugify(unicode(t.cat_name))
            t.save()
        return HttpResponseRedirect('/cat_manager/')

    def get(self,request,*args,**kwargs):
        t=Category.objects.filter(cat_slug=kwargs['cat_slug'])[0]
        csform = CategoryAddEditForm(initial={'cat_name':t.cat_name})
        return render(request,"cat_add_edit.html",{'form':csform})

#Views category delete verify page
class DeleteCat(View):
    def post(self,request,*args,**kwargs):
        if 'yes' in request.POST:
            t=Category.objects.filter(cat_slug=kwargs['cat_slug'])[0]
            t.delete()
        if 'no' in request.POST:
            return HttpResponseRedirect('/cat_manager/')
        return HttpResponseRedirect('/cat_manager/')

    def get(self,request,*args,**kwargs):
        return render(request,"cat_delete.html",{'cat_slug':kwargs['cat_slug']})

############################POST PROCESSES#########################################################

class ShowPostList(View):
    def get(self,request,*args,**kwargs):
        pos=Posts.objects.all()
        catas=Category.objects.all()
        img="null_image"
        if request.user.is_active:
            img_url=UserData.objects.filter(user=request.user)[0]
            if img_url.profile_image:
                img=img_url.profile_image.url
        return render(request,"post_list.html",{"posts":pos,"catas":catas,"imgp":img})



class ShowPostCatList(View):
    def get(self,request,*args,**kwargs):
        pos=Posts.objects.filter(post_cat=Category.objects.filter(cat_slug=kwargs['cat_slug']))
        catas=Category.objects.all()
        img="null_image"
        if request.user.is_active:
            img_url=UserData.objects.filter(user=request.user)[0]
            if img_url.profile_image:
                img=img_url.profile_image.url
        return render(request,"post_cat_list.html",{"posts":pos,"catas":catas,"imgp":img})


class ShowPostDetail(View):
    def get(self,request,*args,**kwargs):
        forl=CommentForm()
        pos=Posts.objects.filter(pk=kwargs['pk'])[0]
        com=Comments.objects.filter(comment_post=pos,comment_isValid=True).order_by('comment_pub_date')
        catas=Category.objects.all()
        img="null_image"
        if request.user.is_active:
            img_url=UserData.objects.filter(user=request.user)[0]
            com=Comments.objects.filter(comment_post=pos).order_by('comment_pub_date')
            if img_url.profile_image:
                img=img_url.profile_image.url
        return render(request,"post_detail.html",{"posts":pos,"com_form":forl,"comments":com,"catas":catas,"imgp":img})

    def post(self,request,*args,**kwargs):
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            cmt = comment_form.save(commit=False)
            cmt.email=request.POST.get('email')
            cmt.comment=request.POST.get('comment')
            cmt.comment_post = Posts.objects.get(pk=request.POST.get('pty'))
            cmt.save()
            if request.POST.get('vote') == 'Like':
                pos=Posts.objects.get(pk=request.POST.get('pty'))
                pos.post_like=pos.post_like+1
                pos.save()
            else:
                pos=Posts.objects.get(pk=request.POST.get('pty'))
                pos.post_dislike=pos.post_dislike+1
                pos.save()

            return HttpResponseRedirect('/category/'+kwargs['cat_slug']+'/'+kwargs['pk'])
        else:
            messages.add_message(request, messages.INFO, 'Enter Valide Mail Address')
            return HttpResponseRedirect('/category/'+kwargs['cat_slug']+'/'+kwargs['pk'])


class CommentVerify(View):
    def get(self,request,*args,**kwargs):
        cmt = Comments.objects.filter(comment_post=kwargs['pka'],pk=kwargs['pkb'])[0]
        cmt.comment_isValid=True
        cmt.save()
        return HttpResponseRedirect('/category/'+kwargs['cat_slug']+'/'+kwargs['pka'])

class CommentDelete(View):
    def get(self,request,*args,**kwargs):
        cmt = Comments.objects.filter(comment_post=kwargs['pka'],pk=kwargs['pkb'])[0]
        cmt.delete()
        return HttpResponseRedirect('/category/'+kwargs['cat_slug']+'/'+kwargs['pka'])


#########################POST MANAGER###############################################################

class ShowPostEditor(View):
    def dispatch(self, request, *args, **kwargs):
        pos=Posts.objects.filter(post_usr=request.user.id)
        return render(request,"post_manager.html",{"postlist":pos})

class AddNewPost(CreateView):
    template_name="post_add_edit.html"
    form_class=PostAddEditForm
    def form_valid(self, form):
        try:
            a = form.save(commit=False)
            a.post_usr = self.request.user

            if not form.cleaned_data['post_cat']:
                if not Category.objects.filter(cat_name='Uncategorized'):
                    ecat_name="Uncategorized"
                    ecat_slug=slugify(unicode(ecat_name))
                    ecat_user=self.request.user
                    a.post_cat=Category.objects.get_or_create(cat_name=ecat_name,cat_slug=ecat_slug,cat_user=ecat_user)[0]
                    print a.post_cat
                else:
                    a.post_cat=Category.objects.get(cat_name='Uncategorized')


            a.save()
            print a
            return HttpResponseRedirect('/post_manager/')
        except IntegrityError as e:
            print e
            return HttpResponse("I don't know what happen")


class EditPost(View):
    def post(self,request,*args,**kwargs):
        csform = PostAddEditForm(data=request.POST)
        if csform.is_valid():
            t=Posts.objects.filter(pk=kwargs['pk'])[0]
            t.post_usr=request.user
            t.post_tittle=request.POST.get('post_tittle')
            t.post_content=request.POST.get('post_content')
            t.post_edit_date=datetime.now()
            t.isEdited=True
            if not csform.cleaned_data['post_cat']:
                if not Category.objects.get(cat_name='Uncategorized'):
                    ecat_name="Uncategorized"
                    ecat_slug=slugify(unicode(ecat_name))
                    ecat_user=self.request.user
                    t.post_cat=Category.objects.get_or_create(cat_name=ecat_name,cat_slug=ecat_slug,cat_user=ecat_user)[0]
                else:
                    t.post_cat=Category.objects.get(cat_name='Uncategorized')
            else:
                t.post_cat=csform.cleaned_data['post_cat']
            t.save()
        return HttpResponseRedirect('/post_manager/')

    def get(self,request,*args,**kwargs):
        t=Posts.objects.filter(pk=kwargs['pk'])[0]
        csform = PostAddEditForm(initial={'post_tittle':t.post_tittle,'post_content':t.post_content,'post_cat':t.post_cat})
        return render(request,"post_add_edit.html",{'form':csform})



class DeletePost(View):
    def post(self,request,*args,**kwargs):
        if 'yes_post' in request.POST:
            t=Posts.objects.filter(pk=kwargs['pk'])[0]
            t.delete()
        if 'no_post' in request.POST:
            return HttpResponseRedirect('/post_manager/')
        return HttpResponseRedirect('/post_manager/')

    def get(self,request,*args,**kwargs):
        return render(request,"post_delete.html",{'pk':kwargs['pk']})


###################################################REST#################################################
class ApiView(View):
    def get(self,request,*args,**kwargs):
        return render(request,"api.html")

class ApiDocView(View):
    def get(self,request,*args,**kwargs):
        return render(request,"apidoc.html")

class CategoryPostApi(ViewSet):
    def list(self, request,ck):
        queryset = Posts.objects.filter(post_cat=Category.objects.filter(pk=ck))
        serializer = PostListSerializer(queryset, many=True)
        return Response(serializer.data)

class CategoryManagerApi(ViewSet):
    authentication_classes = (TokenAuthentication,)
    def list(self,request):
        queryset=Category.objects.filter(~Q(cat_name="Uncategorized"),cat_user=request.user.id)
        serializer=CategoryDetailSerializer(queryset,many=True)
        return Response(serializer.data)

class PostManagerApi(ViewSet):
    authentication_classes = (TokenAuthentication,)
    def list(self,request):
        queryset=Posts.objects.filter(post_usr=request.user.id)
        serializer=PostListSerializer(queryset,many=True)
        return Response(serializer.data)

class PostDetailApi(ViewSet):
    def list(self, request,ck):
        queryset = Posts.objects.filter(pk=ck)
        serializer = PostListSerializer(queryset, many=True)
        return Response(serializer.data)

class CategoryApi(ViewSet):
    authentication_classes = (TokenAuthentication,)
    def list(self, request):
        queryset = Category.objects.all()
        serializer = CategorySerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Category.objects.all()
        cat = get_object_or_404(queryset, pk=pk)
        serializer = CategoryDetailSerializer(cat)
        return Response(serializer.data)

    def create(self, request):
        try:
            if request.user.is_authenticated():
                a=Category()
                a.cat_name=request.DATA['cat_name']
                a.cat_user = self.request.user
                a.cat_slug = slugify(unicode(a.cat_name))
                a.save()
                return Response({"success":"Category successfully created"},status=status.HTTP_200_OK)
            else:
                return Response({"info":"You must login first!"},status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            return Response({"info":e.message},status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        try:
            if request.user.is_authenticated() and Category.objects.get(pk=pk).cat_user.id==request.user.id:
                a=Category.objects.get(pk=pk)
                a.cat_name=request.DATA['cat_name']
                a.cat_user = self.request.user
                a.cat_slug = slugify(unicode(a.cat_name))
                a.save()
                return Response({"success":"Category successfully updated!"},status=status.HTTP_200_OK)
            else:
                return Response({"info":"You must login first! You can only update your categories"},status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            return Response({"info":e.message},status=status.HTTP_400_BAD_REQUEST)


    def destroy(self, request, pk=None):
        try:
            if request.user.is_authenticated() and Category.objects.get(pk=pk).cat_user.id==request.user.id:
                a=Category.objects.get(pk=pk)
                a.delete()
                return Response({"success":"Category Deleted!"},status=status.HTTP_200_OK)
            else:
                return Response({"info":"You must login first! or You can only delete your categories"},status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            return Response({"info":e.message},status=status.HTTP_400_BAD_REQUEST)

class PostApi(ViewSet):
    authentication_classes = (TokenAuthentication,)
    def list(self, request):
        queryset = Posts.objects.all()
        serializer = PostListSerializer(queryset, many=True)
        return Response(serializer.data)


    def retrieve(self, request, pk=None):
        queryset = Posts.objects.all()
        pst = get_object_or_404(queryset, pk=pk)
        serializer = PostDetailSerializer(pst)
        return Response(serializer.data)


    def create(self, request):
            serializer = PostDetailSerializer(data=request.DATA)
            if request.user.is_authenticated():
                if serializer.is_valid():
                    a=Posts()
                    a.post_tittle=serializer.validated_data['post_tittle']
                    a.post_usr = self.request.user
                    a.post_content = request.DATA['post_content']
                    if not request.DATA.get('post_cat'):
                        if not Category.objects.filter(cat_name='Uncategorized'):
                            ecat_name="Uncategorized"
                            ecat_slug=slugify(unicode(ecat_name))
                            ecat_user=self.request.user
                            a.post_cat=Category.objects.get_or_create(cat_name=ecat_name,cat_slug=ecat_slug,cat_user=ecat_user)[0]
                            print a.post_cat
                        else:
                            a.post_cat=Category.objects.get(cat_name='Uncategorized')
                    else:
                        a.post_cat= Category.objects.get(pk=request.DATA.get('post_cat'));
                    a.save()
                    return Response({"success":"Post successfully created!"},status=status.HTTP_200_OK)
                else:
                    return Response({"info":serializer.errors})
            else:
                return Response({"info":"You must login first!"},status=status.HTTP_401_UNAUTHORIZED)


    def update(self, request, pk=None):
        try:
            serializer = PostDetailSerializer(data=request.DATA)
            if request.user.is_authenticated() and Posts.objects.get(pk=pk).post_usr.id == request.user.id:
                if serializer.is_valid():
                    a=Posts.objects.get(pk=pk)
                    a.post_tittle=serializer.validated_data['post_tittle']
                    a.post_usr = self.request.user
                    a.post_content = request.DATA['post_content']
                    if not request.DATA.get('post_cat'):
                        if not Category.objects.filter(cat_name='Uncategorized'):
                            ecat_name="Uncategorized"
                            ecat_slug=slugify(unicode(ecat_name))
                            ecat_user=self.request.user
                            a.post_cat=Category.objects.get_or_create(cat_name=ecat_name,cat_slug=ecat_slug,cat_user=ecat_user)[0]
                            print a.post_cat
                        else:
                            a.post_cat=Category.objects.get(cat_name='Uncategorized')
                    else:
                        a.post_cat= Category.objects.get(pk=request.DATA.get('post_cat'));
                    a.save()
                    return Response({"success":"Post successfully updated!"},status=status.HTTP_200_OK)
                else:
                    return Response({"info":serializer.errors})
            else:
                return Response({"info":"You must login first! or You can only update your posts"},status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            return Response({"info":e.message},status=status.HTTP_400_BAD_REQUEST)


    def destroy(self, request, pk=None):
        try:
            if request.user.is_authenticated() and Posts.objects.get(pk=pk).post_usr.id == request.user.id:
                a=Posts.objects.get(pk=pk)
                a.delete()
                return Response({"success":"Post successfully deleted"},status=status.HTTP_200_OK)
            else:
                return Response({"info":"You must login first! or You can only delete your posts"},status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            return Response({"info":e.message},status=status.HTTP_400_BAD_REQUEST)

class LoginApi(ViewSet):
    def list(self,request):
        return Response({"info":"Enter username and password!"})
    def create(self, request):
        serializer = LoginSerializer(data=request.DATA)
        if serializer.is_valid():
            username = serializer.validated_data.get('username')
            password = serializer.validated_data.get('password')
            user = authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    login(request, user)
                    token = Token.objects.get_or_create(user=user)
                    return Response({'id':User.objects.get(username=serializer.validated_data.get('username')).id,'user':serializer.validated_data.get('username'),'token': token[0].key},status=status.HTTP_200_OK)
                else:
                    return Response({"info":"Account Disabled"},status=status.HTTP_403_FORBIDDEN)
        else:
            return Response({"info":"Enter valid username and password!"},status=status.HTTP_406_NOT_ACCEPTABLE)
        return Response({"info":"Invalid Data!"},status=status.HTTP_403_FORBIDDEN)

class LogoutApi(ViewSet):
    authentication_classes = (TokenAuthentication,)
    def list(self,request):
        if request.user.is_authenticated():
            Token.objects.get(user=request.user).delete()
            logout(request)

            return Response({"success":"User Loged Out and Token deleted!"},status=status.HTTP_200_OK)
        else:
            return Response({"info":"You must login first"},status=status.HTTP_401_UNAUTHORIZED)

class CommentApi(ViewSet):
    authentication_classes = (TokenAuthentication,)
    def list(self,request,ck=None):
        if request.user.is_authenticated():
            queryset=Comments.objects.filter(comment_post=ck)
            serializer=CommentDetailSerializer(queryset,many=True)
        else:
            queryset=Comments.objects.filter(comment_post=ck,comment_isValid=True)
            serializer=CommentSerializer(queryset,many=True)
        return Response(serializer.data)

    def create(self,request,ck=None):
        try:
            serializer=CommentSerializer(data=request.DATA)
            serializer_l=LikeSerizlizer(data=request.DATA)
            cmt = Comments()
            if serializer.is_valid() and serializer_l.is_valid():
                cmt.email=serializer.validated_data.get('email')
                cmt.comment=serializer.validated_data.get('comment')
                cmt.comment_post=Posts.objects.get(pk=ck)
                cmt.save()
                pst=Posts.objects.get(pk=ck)
                if serializer_l.validated_data.get('vote'):
                    pst.post_like=pst.post_like+1
                else:
                    pst.post_dislike=pst.post_dislike+1
                pst.save()
                return Response(Comments.objects.filter(id=cmt.id).values())
            else:
                return Response({"info":"Invalid Post"})
        except Exception as e:
            return Response({"info":e.message},status=status.HTTP_400_BAD_REQUEST)

class CommentVerifyApi(ViewSet):
    authentication_classes = (TokenAuthentication,)
    def list(self,request,ck,pk):
        if request.user.is_authenticated() and Posts.objects.filter(pk=Comments.objects.get(pk=ck).comment_post.id)[0].post_usr.id==request.user.id:
            cmt=Comments.objects.filter(comment_post=pk,pk=ck)[0]
            cmt.comment_isValid=True
            cmt.save()
            return Response({"success":"Comment Verified"},status=status.HTTP_200_OK)
        else:
            return Response({"info":"You must login first or You can only verify your post comments"},status=status.HTTP_401_UNAUTHORIZED)


class CommentDeleteApi(ViewSet):
    authentication_classes = (TokenAuthentication,)
    def list(self,request,ck,pk):
        if request.user.is_authenticated() and Posts.objects.filter(pk=Comments.objects.get(pk=ck).comment_post.id)[0].post_usr.id==request.user.id:
            cmt=Comments.objects.filter(comment_post=pk,pk=ck)[0]
            cmt.delete()
            return Response({"success":"Comment Deleted"},status=status.HTTP_200_OK)
        else:
            return Response({"info":"You must login first or You can only delete your post comments"},status=status.HTTP_401_UNAUTHORIZED)


class RegisterApi(ViewSet):
    authentication_classes = (TokenAuthentication,)
    def list(self,request):
        return Response(User.objects.filter(pk=request.user.id).values())
    def create(self,request):
        try:
            srlz = RegisterSerializer(data=request.DATA)
            if srlz.is_valid():
                usr = User()
                usr_data=UserData()

                usr.username = srlz.validated_data.get('username')
                usr.set_password(srlz.validated_data.get('password'))
                usr.email=srlz.validated_data.get('email')
                if srlz.validated_data.get('first_name'):
                    usr.first_name=srlz.validated_data.get('first_name')
                if srlz.validated_data.get('last_name'):
                    usr.last_name=srlz.validated_data.get('last_name')
                usr.save()
                usr_data.user=User.objects.get(username=srlz.validated_data.get('username'))
                usr_data.save()
                return Response({"info":srlz.validated_data.get('username')+" successfully registered!"},status=status.HTTP_202_ACCEPTED)
            else:
                return Response(srlz.errors,status=status.HTTP_406_NOT_ACCEPTABLE)
        except Exception as e:
            return Response(e.message,status=status.HTTP_400_BAD_REQUEST)

    def update(self,request,pk):
        try:
            srlz_a = RegisterSerializer(data=request.DATA)
            if request.user.is_authenticated():
                if srlz_a.is_valid() and  request.user.id == int(pk):
                    usr = User.objects.get(pk=pk)
                    usr.username=srlz_a.validated_data.get('username')
                    usr.set_password(srlz_a.validated_data.get('password'))
                    usr.email=srlz_a.validated_data.get('email')
                    usr.first_name=srlz_a.validated_data.get('first_name')
                    usr.last_name=srlz_a.validated_data.get('last_name')
                    usr.save()
                    return Response({"success":"Account Settings Changed"})
                else:
                    return Response({"info":"You have to fill username and password for update!"},status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"info":"You must login first"})
        except Exception as e:
            return Response({"info":"You have to fill username and password for update!"},status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None):
        if request.user.id == int(pk):
            queryset = User.objects.get(pk=pk)
            serializer = RegisteredUserSerializer(queryset,many=False)
            return Response(serializer.data)
        else:
            return Response(request.user.id)
