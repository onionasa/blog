from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class UserData(models.Model):
    user = models.ForeignKey(User)
    web=models.CharField(blank=True,default="",max_length=100);
    profile_image=models.ImageField(upload_to='imgs',blank=True,null=True);
def get_sentinel_user():
    return User.objects.get_or_create(username='deleted')[0]


class Category(models.Model):
    cat_name=models.CharField(max_length=100)
    cat_slug=models.SlugField(max_length=100,unique=True)
    cat_create_date=models.DateField(auto_now_add=True)
    cat_user=models.ForeignKey(User,on_delete=models.SET(get_sentinel_user))

    def __str__(self):
        return self.cat_name

def gcat():
    return Category.objects.get_or_create(cat_name='Uncategorized')[0]

class Posts(models.Model):
    post_tittle=models.CharField(max_length=100)
    post_content=models.TextField()
    post_pub_date=models.DateField(auto_now_add=True)
    post_edit_date=models.DateField(auto_now=True)
    post_like=models.IntegerField(default=0,blank=True)
    post_dislike=models.IntegerField(default=0,blank=True)
    isEdited=models.BooleanField(default=False,blank=True)
    post_usr=models.ForeignKey(User,on_delete=models.SET(get_sentinel_user))
    post_cat=models.ForeignKey(Category,on_delete=models.SET(gcat),blank=True,null=True)


class Comments(models.Model):
    email=models.EmailField(max_length=100)
    comment=models.TextField()
    comment_pub_date=models.DateField(auto_now_add=True)
    comment_isValid=models.BooleanField(default=False,blank=True)
    comment_post=models.ForeignKey(Posts,on_delete=models.CASCADE)
