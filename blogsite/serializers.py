from django.contrib.auth.models import User
from rest_framework import serializers
from blogsite.models import Posts, Category, Comments, UserData

__author__ = 'orionasa'



class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model=Category
        fields=['id','cat_name']


class CategoryDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model=Category
        fields=['id','cat_name','cat_slug','cat_create_date']
class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model=Comments
        fields=['id','email','comment']

class PostSerializer(serializers.ModelSerializer):
    comments_set = CommentSerializer(many=True,read_only=True)
    class Meta:
        model=Posts
        fields=['id','post_tittle','post_content','comments_set']

class UserList(serializers.ModelSerializer):
    class Meta:
        model=User
        fields=['username']


class PostListSerializer(serializers.ModelSerializer):
    post_cat = CategorySerializer(many=False,read_only=True)
    post_usr = UserList(many=False,read_only=True)
    class Meta:
        model=Posts
        fields=['id','post_tittle','post_content','post_usr','post_cat','post_like','post_dislike','post_pub_date','post_edit_date']

class PostDetailSerializer(serializers.ModelSerializer):
    post_cat = serializers.PrimaryKeyRelatedField(required=False,read_only=True,allow_null=True)
    post_usr = serializers.PrimaryKeyRelatedField(required=False,read_only=True,allow_null=True)
    class Meta:
        model=Posts
        fields=['id','post_tittle','post_content','post_usr','post_cat','post_like','post_dislike','post_pub_date']

class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True)




class CommentDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model=Comments
        fields=['id','email','comment','comment_isValid']


class LikeSerizlizer(serializers.Serializer):
    vote = serializers.BooleanField(default=True)


class RegisteredUserSerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields=['id','username','password','email','first_name','last_name']

class RegisterSerializer(serializers.Serializer):
    first_name=serializers.CharField(required=False,default="",allow_blank=True)
    last_name=serializers.CharField(required=False,default="",allow_blank=True)
    username=serializers.CharField(required=True)
    password=serializers.CharField(required=True)
    email=serializers.EmailField(required=True)


class RegisterProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model=UserData
        fields=['web','profile_image']
