# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import blogsite.models


class Migration(migrations.Migration):

    dependencies = [
        ('blogsite', '0004_auto_20150622_0912'),
    ]

    operations = [
        migrations.AlterField(
            model_name='posts',
            name='post_cat',
            field=models.ForeignKey(on_delete=models.SET(blogsite.models.gcat), blank=True, to='blogsite.Category', null=True),
        ),
    ]
