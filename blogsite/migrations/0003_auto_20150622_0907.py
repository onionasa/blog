# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import blogsite.models


class Migration(migrations.Migration):

    dependencies = [
        ('blogsite', '0002_auto_20150622_0841'),
    ]

    operations = [
        migrations.AlterField(
            model_name='posts',
            name='post_cat',
            field=models.ForeignKey(on_delete=models.SET(blogsite.models.gcat), blank=True, to='blogsite.Category'),
        ),
        migrations.AlterField(
            model_name='posts',
            name='post_usr',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.SET(blogsite.models.get_sentinel_user)),
        ),
    ]
