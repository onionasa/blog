# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import blogsite.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cat_name', models.CharField(max_length=100)),
                ('cat_slug', models.SlugField(unique=True, max_length=100)),
                ('cat_create_date', models.DateField(auto_now_add=True)),
                ('cat_usr', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.SET(blogsite.models.get_sentinel_user))),
            ],
        ),
        migrations.CreateModel(
            name='Comments',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=100)),
                ('comment', models.TextField()),
                ('comment_pub_date', models.DateField(auto_now_add=True)),
                ('comment_isValid', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Posts',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('post_tittle', models.CharField(max_length=100)),
                ('post_content', models.TextField()),
                ('post_pub_date', models.DateField(auto_now_add=True)),
                ('post_edit_date', models.DateField(auto_now=True)),
                ('post_like', models.IntegerField(default=0, blank=True)),
                ('post_dislike', models.IntegerField(default=0, blank=True)),
                ('isEdited', models.BooleanField(default=False)),
                ('post_cat', models.ForeignKey(on_delete=models.SET(blogsite.models.gcat), default=models.SET(blogsite.models.gcat), blank=True, to='blogsite.Category')),
                ('post_usr', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.SET(blogsite.models.gcat))),
            ],
        ),
        migrations.CreateModel(
            name='UserData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('web', models.CharField(default=b'', max_length=100, blank=True)),
                ('profile_image', models.ImageField(null=True, upload_to=b'imgs', blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='comments',
            name='comment_post',
            field=models.ForeignKey(to='blogsite.Posts'),
        ),
    ]
