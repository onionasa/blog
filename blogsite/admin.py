from django.contrib import admin

# Register your models here.
from blogsite.models import UserData, Category, Posts, Comments

admin.site.register(Category)
admin.site.register(UserData)
admin.site.register(Posts)
admin.site.register(Comments)