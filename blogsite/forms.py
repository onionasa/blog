from django.contrib.auth.models import User
from django import forms
from django.forms.widgets import TextInput, Textarea, Select, PasswordInput, EmailInput
from blogsite.models import UserData, Category, Posts, Comments


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'password', 'email')

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget = TextInput(attrs={
            'class': 'form-control'})

        self.fields['password'].widget = PasswordInput(attrs={
            'class': 'form-control'})

        self.fields['email'].widget = EmailInput(attrs={
            'class': 'form-control'})


class SettingsForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'password', 'email', 'first_name', 'last_name')

    def __init__(self, *args, **kwargs):
        super(SettingsForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget = TextInput(attrs={
            'class': 'form-control'})

        self.fields['first_name'].widget = TextInput(attrs={
            'class': 'form-control'})

        self.fields['last_name'].widget = TextInput(attrs={
            'class': 'form-control'})

        self.fields['password'].widget = PasswordInput(attrs={
            'class': 'form-control'})

        self.fields['email'].widget = EmailInput(attrs={
            'class': 'form-control'})


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserData
        fields = ('web', 'profile_image')

    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)
        self.fields['web'].widget = TextInput(attrs={
            'class': 'form-control'})


class CategoryAddEditForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['cat_name']

    def __init__(self, *args, **kwargs):
        super(CategoryAddEditForm, self).__init__(*args, **kwargs)
        self.fields['cat_name'].widget = TextInput(attrs={
            'class': 'form-control'})


class PostAddEditForm(forms.ModelForm):
    class Meta:
        model = Posts
        fields = ['post_tittle', 'post_content', 'post_cat']

    def __init__(self, *args, **kwargs):
        super(PostAddEditForm, self).__init__(*args, **kwargs)
        self.fields['post_tittle'].widget = TextInput(attrs={
            'class': 'form-control'})
        self.fields['post_content'].widget = Textarea(attrs={
            'class': 'form-control'})


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comments
        fields = ['email', 'comment']

    def __init__(self, *args, **kwargs):
        super(CommentForm, self).__init__(*args, **kwargs)
        self.fields['comment'].widget = Textarea(attrs={
            'class': 'form-control'})
        self.fields['email'].widget = TextInput(attrs={
            'class': 'form-control'})


